resource "google_compute_firewall" "default" {
  name    = "firewall-jenkins"
  network = "${google_compute_network.test.name}"

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["80", "443"]
  }

  target_tags = ["jenkins"]
  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_network" "test" {
  name = "test-network"
}
