terraform {
  backend "gcs" {
    bucket  = "equifax-demo-tfstate"
    prefix  = "terraform/state/jenkins-on-gcp"
  }
}
