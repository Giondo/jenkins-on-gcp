output "vmname" {
	value = "${google_compute_instance.jenkinsvm.name}"
}
output "ExternalIP" {
	value = "${google_compute_address.static_ext_ip.address}"
}
