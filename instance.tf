resource "google_compute_instance" "jenkinsvm" {
    name         = "jenkins"
    machine_type = "n1-standard-2"
    tags = ["jenkins", "equipo1"]

    boot_disk {
        initialize_params {
          //image = "${resource.google_compute_image.jenkinsimage.self_link}"
          image = "https://www.googleapis.com/compute/v1/projects/click-to-deploy-images/global/images/jenkins-v20190923"

        }
    }

    network_interface {
      network = "${google_compute_network.test.name}"
      access_config {
            nat_ip = "${google_compute_address.static_ext_ip.address}"
        }
      }

    metadata = {
      jenkins-admin-password = "admin"
      }


    //metadata_startup_script = "${file("scripts/init_script-jenkins.sh")}"


}
